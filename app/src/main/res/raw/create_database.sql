-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2017-06-25 01:36:56.421

-- tables
-- Table: notificator
CREATE TABLE notificator (
    id integer NOT NULL CONSTRAINT notificator_pk PRIMARY KEY,
    higher_price double NOT NULL,
    lower_price double NOT NULL,
    currency varchar(30) NOT NULL,
    send_email integer NOT NULL
);

-- Table: ticker
CREATE TABLE ticker (
    id integer NOT NULL CONSTRAINT ticker_pk PRIMARY KEY,
    high double NOT NULL,
    buy double NOT NULL,
    sell double NOT NULL,
    last_price double NOT NULL,
    low double NOT NULL,
    currency varchar(30) NOT NULL,
    exchange varchar(30) NOT NULL
);

-- End of file.

