package com.example.metzger.bitcoinnotificator;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.metzger.bitcoinnotificator.adapters.OrderbookTableDataAdapter;
import com.example.metzger.bitcoinnotificator.models.Currency;
import com.example.metzger.bitcoinnotificator.models.Exchange;
import com.example.metzger.bitcoinnotificator.models.orderbook.Orderbook;
import com.example.metzger.bitcoinnotificator.models.orderbook.OrderbookLineVO;
import com.example.metzger.bitcoinnotificator.models.tickers.Ticker;
import com.example.metzger.bitcoinnotificator.services.BitcoinService;
import com.example.metzger.bitcoinnotificator.services.BitcoinServiceFactory;
import com.example.metzger.bitcoinnotificator.services.ServiceFactory;

import java.util.ArrayList;
import java.util.List;

import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class OrderbookActivity extends AppCompatActivity {

    TableView<OrderbookLineVO> tableOrderbook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderbook);
        tableOrderbook = (TableView<OrderbookLineVO>) findViewById(R.id.tableOrderbook);

        Exchange exchange = (Exchange) getIntent().getSerializableExtra("exchange");

        SimpleTableHeaderAdapter headerAdapter = new SimpleTableHeaderAdapter(this, "$ Compra", "Quantidade", "$ Venda", "Quantidade");
        tableOrderbook.setHeaderAdapter(headerAdapter);

        OrderbookFetcher fetcher = new OrderbookFetcher();
        fetcher.execute(exchange);
    }


    private class OrderbookFetcher extends AsyncTask<Exchange, Void, List<OrderbookLineVO>> {

        @Override
        protected List<OrderbookLineVO> doInBackground(Exchange... params) {

            if (params.length < 1 || params.length > 1)
                throw new IndexOutOfBoundsException();

            Exchange exchange = params[0];

            BitcoinService service = BitcoinServiceFactory.getInstance().makeBitcoinService(exchange);

            Orderbook orderbook = service.getOrderbook();

            return orderbook.toOrderbookVOList();
        }

        @Override
        protected void onPostExecute(List<OrderbookLineVO> orderbooks) {
            super.onPostExecute(orderbooks);
            tableOrderbook.setDataAdapter(new OrderbookTableDataAdapter(OrderbookActivity.this, orderbooks));
        }
    }
}
