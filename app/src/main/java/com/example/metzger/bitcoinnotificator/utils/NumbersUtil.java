package com.example.metzger.bitcoinnotificator.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by LeoMetzger on 25/06/2017.
 */

public class NumbersUtil {

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
