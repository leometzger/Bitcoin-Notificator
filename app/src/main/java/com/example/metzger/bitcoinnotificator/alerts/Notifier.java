package com.example.metzger.bitcoinnotificator.alerts;

import android.content.Context;

public interface Notifier {

    void send(Context context, int type, boolean email);
}
