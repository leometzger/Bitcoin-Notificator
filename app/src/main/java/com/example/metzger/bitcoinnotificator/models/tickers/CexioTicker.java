package com.example.metzger.bitcoinnotificator.models.tickers;

import com.example.metzger.bitcoinnotificator.models.Currency;
import com.example.metzger.bitcoinnotificator.models.Exchange;

public class CexioTicker extends Ticker {

    public CexioTicker() {
        currency = Currency.DOLLAR;
        exchange = Exchange.CEXIO;
    }
}
