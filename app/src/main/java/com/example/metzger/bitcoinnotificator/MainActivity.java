package com.example.metzger.bitcoinnotificator;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.metzger.bitcoinnotificator.alerts.Notifier;
import com.example.metzger.bitcoinnotificator.alerts.NotifierFactory;
import com.example.metzger.bitcoinnotificator.models.Exchange;
import com.example.metzger.bitcoinnotificator.models.tickers.Ticker;
import com.example.metzger.bitcoinnotificator.services.BitcoinService;
import com.example.metzger.bitcoinnotificator.services.CexioService;
import com.example.metzger.bitcoinnotificator.services.FoxbitService;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView foxbitMin, foxbitMax, foxbitActual;
    TextView cexioMin, cexioMax, cexioActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.loadComponents();
        new Fetcher().execute(new FoxbitService(), new CexioService());

        Notifier notifier = NotifierFactory.getInstance().makeNotifier(Exchange.CEXIO);
        notifier.send(this, 0, false);
    }

    public void configureNotifications(View view){
        Intent intent = new Intent(this, ConfigureNotificationsActivity.class);
        startActivity(intent);
    }

    public void viewHistory(View view){
        Intent intent = new Intent(this, TickerHistoryActivity.class);
        startActivity(intent);
    }

    public void viewOrderbook(View view){
        Exchange exchange = null;
        switch (view.getId()){
            case R.id.foxbitOrderbook:
                exchange = Exchange.FOXBIT;
                break;
            case R.id.cexioOrderbook:
                exchange = Exchange.CEXIO;
                break;
        }
        Intent intent = new Intent(this, OrderbookActivity.class);
        intent.putExtra("exchange", exchange);
        startActivity(intent);
    }

    private void loadComponents(){
        foxbitMin = (TextView) findViewById(R.id.foxbitMin);
        foxbitMax = (TextView) findViewById(R.id.foxbitMax);
        foxbitActual = (TextView) findViewById(R.id.foxbitActual);

        cexioMin = (TextView) findViewById(R.id.cexioMin);
        cexioMax = (TextView) findViewById(R.id.cexioMax);
        cexioActual = (TextView) findViewById(R.id.cexioActual);
    }

    private class Fetcher extends AsyncTask<BitcoinService, Void, List<Ticker>> {

        @Override
        protected List<Ticker> doInBackground(BitcoinService... services) {

            List<Ticker> tickers = new ArrayList<>();
            for(int i = 0; i < services.length; i++) {
                try {
                    Ticker ticker = services[i].getTicker();
                    if(ticker != null)
                        tickers.add(ticker);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return tickers;
        }

        @Override
        protected void onPostExecute(List<Ticker> tickers) {
            for (Ticker ticker : tickers) {
                if(tickers == null)
                    continue;
                switch (ticker.getExchange()) {
                    case CEXIO:
                        fillExchange(ticker, cexioMax, cexioMin, cexioActual);
                        break;
                    case FOXBIT:
                        fillExchange(ticker, foxbitMax, foxbitMin, foxbitActual);
                        break;
                }
            }
        }

        private void fillExchange(Ticker ticker, TextView max, TextView min, TextView actual){
            String maxPrice = formatTicker(ticker.getHigh(), ticker);
            String minPrice = formatTicker(ticker.getLow(), ticker);
            String actualPrice = formatTicker(ticker.getLast(), ticker);

            max.setText(maxPrice);
            min.setText(minPrice);
            actual.setText(actualPrice);
        }

        private String formatTicker(double value, Ticker ticker){
            return String.format("%.2f ", value).concat(ticker.getCurrency().getText());
        }
    }
}
