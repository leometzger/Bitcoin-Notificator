package com.example.metzger.bitcoinnotificator.tickers;

import com.example.metzger.bitcoinnotificator.models.Exchange;

/**
 * Created by LeoMetzger on 26/06/2017.
 */

public class CexioMonitor extends Monitor {

    public CexioMonitor() {
        this.exchange = Exchange.CEXIO;
    }
}
