package com.example.metzger.bitcoinnotificator.models;


import com.example.metzger.bitcoinnotificator.models.tickers.FoxbitTicker;

public class User {

    private String username;
    private String password;
    private String name;
    private FoxbitTicker ticker;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FoxbitTicker getTicker() {
        return ticker;
    }

    public void setTicker(FoxbitTicker ticker) {
        this.ticker = ticker;
    }
}
