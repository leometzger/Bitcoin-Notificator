package com.example.metzger.bitcoinnotificator.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.metzger.bitcoinnotificator.R;

public class BitcoinNotificatorSQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "BitcoinNotificator";

    private static final int DATABASE_VERSION = 1;

    public BitcoinNotificatorSQLiteHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
