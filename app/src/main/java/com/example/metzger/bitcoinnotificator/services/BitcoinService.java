package com.example.metzger.bitcoinnotificator.services;

import com.example.metzger.bitcoinnotificator.models.orderbook.Orderbook;
import com.example.metzger.bitcoinnotificator.models.tickers.Ticker;

public interface BitcoinService {
    Ticker getTicker();
    Orderbook getOrderbook();
}
