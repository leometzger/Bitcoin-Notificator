package com.example.metzger.bitcoinnotificator.models.tickers;

import com.example.metzger.bitcoinnotificator.models.Currency;
import com.example.metzger.bitcoinnotificator.models.Exchange;

public class FoxbitTicker extends Ticker {

    public FoxbitTicker(){
        currency = Currency.REAL;
        exchange = Exchange.FOXBIT;
    }
}
