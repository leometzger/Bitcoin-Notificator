package com.example.metzger.bitcoinnotificator.alerts;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.example.metzger.bitcoinnotificator.MainActivity;
import com.example.metzger.bitcoinnotificator.R;

import java.util.Observable;
import java.util.Observer;

public class CexioNotifier implements Notifier {

    private static final String MIN_MESSAGE = "(CEXIO) Valor mínimo foi atingido";

    private static final String MAX_MESSAGE = "(CEXIO) Valor máximo foi atingido";

    @Override
    public void send(Context context, int type, boolean email) {
        String message = type == 0 ? MIN_MESSAGE : MAX_MESSAGE;

        Intent resultIntent = new Intent(context, MainActivity.class);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        Notification notification = buildNotification(context, message, resultPendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService
                        (Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, notification);
    }

    private Notification buildNotification(Context context, String message, PendingIntent pendingIntent){
        return new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.cexio_logo)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.cexio_logo))
                .setContentTitle("Cexio Notification")
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .build();
    }
}
