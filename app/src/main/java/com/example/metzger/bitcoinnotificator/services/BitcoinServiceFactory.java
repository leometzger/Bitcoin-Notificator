package com.example.metzger.bitcoinnotificator.services;

import com.example.metzger.bitcoinnotificator.alerts.NotifierFactory;
import com.example.metzger.bitcoinnotificator.models.Exchange;

/**
 * Created by LeoMetzger on 25/06/2017.
 */

public class BitcoinServiceFactory implements ServiceFactory {


    private static final BitcoinServiceFactory ourInstance = new BitcoinServiceFactory();

    public static BitcoinServiceFactory getInstance() {
        return ourInstance;
    }

    private BitcoinServiceFactory() {
    }

    @Override
    public BitcoinService makeBitcoinService(Exchange exchange) {
        switch (exchange){
            case FOXBIT:
                return new FoxbitService();
            case CEXIO:
                return new CexioService();
        }
        return null;
    }
}
