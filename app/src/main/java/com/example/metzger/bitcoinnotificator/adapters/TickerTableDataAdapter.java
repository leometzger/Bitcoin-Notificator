package com.example.metzger.bitcoinnotificator.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.metzger.bitcoinnotificator.models.tickers.Ticker;

import java.util.List;

import de.codecrafters.tableview.TableDataAdapter;

/**
 * Created by LeoMetzger on 24/06/2017.
 */
public class TickerTableDataAdapter extends TableDataAdapter<Ticker> {

    public TickerTableDataAdapter(Context context, List<Ticker> values){
        super(context, values);
    }

    @Override
    public View getCellView(int rowIndex, int columnIndex, ViewGroup parentView) {
        Ticker ticker = getRowData(rowIndex);
        View renderView = null;

        switch (columnIndex){
            case 0:
                renderView = renderPrice(ticker);
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
        }

        return renderView;
    }

    private View renderPrice(Ticker ticker){
        TextView tv = new TextView(getContext());
        tv.setText(String.valueOf(ticker.getLast()));
        return tv;
    }
}
