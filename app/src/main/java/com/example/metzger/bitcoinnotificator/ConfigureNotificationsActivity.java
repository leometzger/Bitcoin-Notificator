package com.example.metzger.bitcoinnotificator;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.metzger.bitcoinnotificator.models.Currency;
import com.example.metzger.bitcoinnotificator.models.Notificator;
import com.example.metzger.bitcoinnotificator.tickers.CexioMonitor;
import com.example.metzger.bitcoinnotificator.tickers.FoxbitMonitor;

import java.util.Calendar;


public class ConfigureNotificationsActivity extends AppCompatActivity {

    TextView higherBound, lowerBound;
    Spinner currency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure_notifications);
        loadComponents();
    }

    private void loadComponents(){

        higherBound = (TextView) findViewById(R.id.higerbound);
        lowerBound = (TextView) findViewById(R.id.lowerbound);

        currency = (Spinner) findViewById(R.id.currency);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.currency_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currency.setAdapter(adapter);
    }

    private boolean validateNotificator(){

        String numberPattern = "[0-9]+[\\.|,]?[0-9]+$";
        String msg = null;

        if(higherBound.getText().length() == 0 || lowerBound.getText().length() == 0)
            msg = "Verifique se todos os números foram preenchidos";

        else if(!higherBound.getText().toString().matches(numberPattern) ||
            !lowerBound.getText().toString().matches(numberPattern))
            msg = "Verifique se os numeros estão no formato correto.";

        if(msg != null) {
            Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public void save(View view){

        if(!validateNotificator())
            return;

        AlarmManager alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Currency cur = currency.getSelectedItem().toString() == "Real (R$)"
                ? Currency.REAL
                : Currency.DOLLAR;

        Notificator notificator = new Notificator();
        notificator.setCurrency(cur);

        notificator.setLowerPrice(Double.parseDouble(
                lowerBound.getText()
                .toString()
                .replace(",", "."))
        );

        notificator.setHigherPrice(Double.parseDouble(
                higherBound.getText()
                .toString()
                .replace(",", "."))
        );

        Intent intent;

        if(cur == Currency.REAL)
            intent = new Intent(this, FoxbitMonitor.class);
        else
            intent = new Intent(this, CexioMonitor.class);

        intent.putExtra("NOTIFICATOR", notificator);

        PendingIntent alarmIntent = PendingIntent.getBroadcast(getBaseContext(), 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.SECOND, 3);

        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 60 * 60 * 1000, alarmIntent);
        Toast.makeText(getBaseContext(), "Notificador salvo com sucesso", Toast.LENGTH_SHORT).show();
        finish();
    }
}
