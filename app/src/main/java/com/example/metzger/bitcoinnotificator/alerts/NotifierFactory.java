package com.example.metzger.bitcoinnotificator.alerts;

import com.example.metzger.bitcoinnotificator.models.Exchange;

/**
 * Created by LeoMetzger on 25/06/2017.
 */

public class NotifierFactory {

    private static final NotifierFactory ourInstance = new NotifierFactory();

    public static NotifierFactory getInstance() {
        return ourInstance;
    }

    private NotifierFactory() {
    }

    public Notifier makeNotifier(Exchange exchange) {
        switch (exchange){
            case FOXBIT:
                return new FoxbitNotifier();
            case CEXIO:
                return new CexioNotifier();
        }
        return null;
    }
}
