package com.example.metzger.bitcoinnotificator.services;

import com.example.metzger.bitcoinnotificator.models.orderbook.CexioOrderbook;
import com.example.metzger.bitcoinnotificator.models.orderbook.Orderbook;
import com.example.metzger.bitcoinnotificator.models.tickers.CexioTicker;
import com.example.metzger.bitcoinnotificator.models.tickers.Ticker;

public class CexioService implements BitcoinService {

    public static final String CEX_IO_TICKER_URL = "https://cex.io/api/ticker/BTC/USD";

    public static final String CEX_IO_ORDERBOOK_URL = "https://cex.io/api/order_book/BTC/USD/?depth=30";

    @Override
    public Ticker getTicker() {
        HttpHelper<CexioTicker> http = new HttpHelper<CexioTicker>(CexioTicker.class);
        return http.doGet(CEX_IO_TICKER_URL);
    }

    @Override
    public Orderbook getOrderbook() {
        HttpHelper<CexioOrderbook> http = new HttpHelper<>(CexioOrderbook.class);
        return http.doGet(CEX_IO_ORDERBOOK_URL);
    }
}
