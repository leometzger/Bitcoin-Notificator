package com.example.metzger.bitcoinnotificator.models;

/**
 * Created by LeoMetzger on 25/06/2017.
 */

public enum Currency {
    REAL("R$"),
    DOLLAR("$");

    String text;

    Currency(String text){
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
