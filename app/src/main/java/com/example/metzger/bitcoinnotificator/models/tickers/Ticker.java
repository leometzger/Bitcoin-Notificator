package com.example.metzger.bitcoinnotificator.models.tickers;

import com.example.metzger.bitcoinnotificator.models.Currency;
import com.example.metzger.bitcoinnotificator.models.Exchange;

/**
 * Created by LeoMetzger on 25/06/2017.
 */

public abstract class Ticker {

    private double high;

    private double low;

    private double last;

    private double sell;

    private double buy;

    protected Currency currency;

    protected Exchange exchange;

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getLast() {
        return last;
    }

    public void setLast(double last) {
        this.last = last;
    }

    public double getSell() {
        return sell;
    }

    public void setSell(double sell) {
        this.sell = sell;
    }

    public double getBuy() {
        return buy;
    }

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }
}
