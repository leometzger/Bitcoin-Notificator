package com.example.metzger.bitcoinnotificator.models.orderbook;

import com.example.metzger.bitcoinnotificator.models.Exchange;

public class FoxbitOrderbook extends Orderbook {

    public FoxbitOrderbook(){
        exchange = Exchange.FOXBIT;
    }
}
