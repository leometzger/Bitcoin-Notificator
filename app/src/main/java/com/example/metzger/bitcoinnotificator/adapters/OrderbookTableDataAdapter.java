package com.example.metzger.bitcoinnotificator.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.metzger.bitcoinnotificator.models.orderbook.OrderbookLineVO;

import java.util.List;

import de.codecrafters.tableview.TableDataAdapter;

public class OrderbookTableDataAdapter extends TableDataAdapter<OrderbookLineVO> {

    public OrderbookTableDataAdapter(Context context, List<OrderbookLineVO> data) {
        super(context, data);
    }

    @Override
    public View getCellView(int rowIndex, int columnIndex, ViewGroup parentView) {
        OrderbookLineVO orderbook = getRowData(rowIndex);
        View renderview = null;

        switch (columnIndex){
            case 0:
                renderview = renderBidPrice(orderbook);
                break;
            case 1:
                renderview = renderBidQuantity(orderbook);
                break;
            case 2:
                renderview = renderAskPrice(orderbook);
                break;
            case 3:
                renderview = renderAskQuantity(orderbook);
                break;
        }
        return renderview;
    }

    private View renderAskPrice(OrderbookLineVO orderbook) {
        TextView tv = new TextView(getContext());
        tv.setText(String.valueOf(orderbook.getAskPrice())
                .concat(" " + orderbook.getCurrency().getText()));
        tv.setBackgroundColor(Color.RED);
        return tv;
    }

    private View renderBidPrice(OrderbookLineVO orderbook) {
        TextView tv = new TextView(getContext());
        tv.setText(String.valueOf(orderbook.getBidPrice())
                .concat(" " + orderbook.getCurrency().getText()));
        tv.setBackgroundColor(Color.GREEN);
        return tv;
    }

    private View renderAskQuantity(OrderbookLineVO orderbook) {
        TextView tv = new TextView(getContext());
        tv.setText(String.valueOf(orderbook.getAskQuantity()));
        return tv;
    }

    private View renderBidQuantity(OrderbookLineVO orderbook) {
        TextView tv = new TextView(getContext());
        tv.setText(String.valueOf(orderbook.getBidQuantity()));
        return tv;
    }

}
