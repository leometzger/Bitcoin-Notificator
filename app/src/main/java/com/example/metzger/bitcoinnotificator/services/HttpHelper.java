package com.example.metzger.bitcoinnotificator.services;

import com.example.metzger.bitcoinnotificator.models.tickers.CexioTicker;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by metzger on 11/06/17.
 */

public class HttpHelper<T> {

    private OkHttpClient client;

    private final Class<T> type;

    public HttpHelper(Class<T> type){
        this.client = new OkHttpClient();
        this.type = type;
    }

    public T doGet(String url) {
        try {
            Request req = new  Request.Builder()
                    .url(url)
                    .build();

            Response resp = client.newCall(req).execute();
            String response = resp.body().string();
            Gson parser = new Gson();

            return parser.fromJson(response, type);
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
