package com.example.metzger.bitcoinnotificator.tickers;

import com.example.metzger.bitcoinnotificator.models.Exchange;

/**
 * Created by LeoMetzger on 26/06/2017.
 */

public class FoxbitMonitor extends Monitor {

    public FoxbitMonitor() {
        this.exchange = Exchange.FOXBIT;
    }
}
