package com.example.metzger.bitcoinnotificator.tickers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.metzger.bitcoinnotificator.alerts.Notifier;
import com.example.metzger.bitcoinnotificator.alerts.NotifierFactory;
import com.example.metzger.bitcoinnotificator.models.Exchange;
import com.example.metzger.bitcoinnotificator.models.Notificator;
import com.example.metzger.bitcoinnotificator.models.tickers.Ticker;
import com.example.metzger.bitcoinnotificator.services.BitcoinService;
import com.example.metzger.bitcoinnotificator.services.BitcoinServiceFactory;

/**
 * Created by LeoMetzger on 26/06/2017.
 */

public abstract class Monitor extends BroadcastReceiver {

    protected Exchange exchange;

    @Override
    public void onReceive(Context context, Intent intent) {

        BitcoinService service = BitcoinServiceFactory.getInstance().makeBitcoinService(exchange);

        Notifier notifier = NotifierFactory.getInstance().makeNotifier(exchange);

        Ticker ticker = service.getTicker();

        Notificator notificator = (Notificator) intent.getSerializableExtra("NOTIFICATOR");

        if(notificator.getLowerPrice() >= ticker.getLast())
            notifier.send(context, 0, true);

        if(notificator.getHigherPrice() <= ticker.getLast())
            notifier.send(context, 1, true);
    }

}
