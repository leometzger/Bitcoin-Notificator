package com.example.metzger.bitcoinnotificator.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.example.metzger.bitcoinnotificator.models.orderbook.FoxbitOrderbook;
import com.example.metzger.bitcoinnotificator.models.orderbook.Orderbook;
import com.example.metzger.bitcoinnotificator.models.tickers.FoxbitTicker;
import com.example.metzger.bitcoinnotificator.models.tickers.Ticker;
import com.google.gson.Gson;

public class FoxbitService implements BitcoinService {

    public static final String FOXBIT_ORDERBOOK_URL = "https://api.blinktrade.com/api/v1/BRL/orderbook";

    public static final String FOXBIT_TICKER_URL = "https://api.blinktrade.com/api/v1/BRL/ticker";

    @Override
    public Ticker getTicker() {
        HttpHelper<FoxbitTicker> http = new HttpHelper<>(FoxbitTicker.class);
        return http.doGet(FOXBIT_TICKER_URL);
    }

    @Override
    public Orderbook getOrderbook() {
        HttpHelper<FoxbitOrderbook> http = new HttpHelper<>(FoxbitOrderbook.class);
        return http.doGet(FOXBIT_ORDERBOOK_URL);
    }

}
