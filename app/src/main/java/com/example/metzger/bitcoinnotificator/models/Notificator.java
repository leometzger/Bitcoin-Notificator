package com.example.metzger.bitcoinnotificator.models;

import android.os.Parcel;

import java.io.Serializable;

public class Notificator implements Serializable {

    public int id;

    public double higherPrice;

    public double lowerPrice;

    public Currency currency;

    public boolean sendEmail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getHigherPrice() {
        return higherPrice;
    }

    public void setHigherPrice(double higherPrice) {
        this.higherPrice = higherPrice;
    }

    public double getLowerPrice() {
        return lowerPrice;
    }

    public void setLowerPrice(double lowerPrice) {
        this.lowerPrice = lowerPrice;
    }

    public boolean isSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
