package com.example.metzger.bitcoinnotificator.models.orderbook;

import com.example.metzger.bitcoinnotificator.models.Exchange;
import com.example.metzger.bitcoinnotificator.utils.NumbersUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by LeoMetzger on 25/06/2017.
 */

public abstract class Orderbook {

    private String pair;

    private double[][] bids;

    private double[][] asks;

    protected Exchange exchange;

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public double[][] getBids() {
        return bids;
    }

    public void setBids(double[][] bids) {
        this.bids = bids;
    }

    public double[][] getAsks() {
        return asks;
    }

    public void setAsks(double[][] asks) {
        this.asks = asks;
    }

    public Exchange getExchange() {
        return exchange;
    }

    public void setExchange(Exchange exchange) {
        this.exchange = exchange;
    }

    public List<OrderbookLineVO> toOrderbookVOList(){

        List<OrderbookLineVO> orderbooks = new ArrayList<>();
        int i = 0;

        while (getBids().length > i && getAsks().length > i){

            OrderbookLineVO vo = new OrderbookLineVO();

            vo.setCurrency(exchange.getCurrency());

            vo.setAskPrice(NumbersUtil.round(getAsks()[i][0], 2));
            vo.setAskQuantity(getAsks()[i][1]);

            vo.setBidPrice(NumbersUtil.round(getBids()[i][0], 2));
            vo.setBidQuantity(getBids()[i][1]);

            orderbooks.add(vo);

            i++;
        }

        return orderbooks;
    }
}
