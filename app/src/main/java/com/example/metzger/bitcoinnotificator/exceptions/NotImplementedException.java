package com.example.metzger.bitcoinnotificator.exceptions;

public class NotImplementedException extends Exception {

    public NotImplementedException(String message) {
        super("Method was not implemented.");
    }
}
