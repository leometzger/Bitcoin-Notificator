package com.example.metzger.bitcoinnotificator.models.orderbook;

import com.example.metzger.bitcoinnotificator.models.Currency;

/**
 * Created by LeoMetzger on 25/06/2017.
 */

public class OrderbookLineVO {

    private double bidPrice;

    private double bidQuantity;

    private double askPrice;

    private double askQuantity;

    private Currency currency;

    public OrderbookLineVO(){}

    public double getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(double bidPrice) {
        this.bidPrice = bidPrice;
    }

    public double getBidQuantity() {
        return bidQuantity;
    }

    public void setBidQuantity(double bidQuantity) {
        this.bidQuantity = bidQuantity;
    }

    public double getAskPrice() {
        return askPrice;
    }

    public void setAskPrice(double askPrice) {
        this.askPrice = askPrice;
    }

    public double getAskQuantity() {
        return askQuantity;
    }

    public void setAskQuantity(double askQuantity) {
        this.askQuantity = askQuantity;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
