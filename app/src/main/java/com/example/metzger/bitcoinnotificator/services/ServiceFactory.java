package com.example.metzger.bitcoinnotificator.services;

import com.example.metzger.bitcoinnotificator.models.Exchange;

/**
 * Created by LeoMetzger on 25/06/2017.
 */

public interface ServiceFactory {

    BitcoinService makeBitcoinService(Exchange exchange);

}
