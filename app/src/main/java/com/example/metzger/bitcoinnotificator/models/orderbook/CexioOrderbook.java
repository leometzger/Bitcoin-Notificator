package com.example.metzger.bitcoinnotificator.models.orderbook;

import com.example.metzger.bitcoinnotificator.models.Exchange;

/**
 * Created by LeoMetzger on 25/06/2017.
 */

public class CexioOrderbook extends Orderbook {

    public CexioOrderbook(){
        exchange = Exchange.CEXIO;
    }

    private String sellTotal;

    private String buyTotal;

    public String getSellTotal() {
        return sellTotal;
    }

    public void setSellTotal(String sellTotal) {
        this.sellTotal = sellTotal;
    }

    public String getBuyTotal() {
        return buyTotal;
    }

    public void setBuyTotal(String buyTotal) {
        this.buyTotal = buyTotal;
    }
}
