package com.example.metzger.bitcoinnotificator.models;

public enum Exchange {
    FOXBIT("foxbit", Currency.REAL),
    CEXIO("cexio", Currency.DOLLAR);

    private final String text;
    private final Currency currency;

    Exchange(final String text, Currency currency) {
        this.text = text;
        this.currency = currency;
    }

    public String getText() {
        return text;
    }

    public Currency getCurrency() { return currency; }

}
