# Bitcoin Notificator

![Index](/prints/Index.JPG)
![Configure Notification](/prints/ConfigNotification.JPG)
![Notification](/prints/Notification.JPG)


**Bitcoin Notificator** é um aplicativo que monitora algumas exchanges com o objetivo de avisar quando houver algumas mudanças no preço do bitcoin, baseado em sua configuração.

O preço é configurável e baseado na moeda utilizada pela **exchange**. 

### Notificações

Tipos de notificação abordadas possíveis:

- [x] Android Notification - Notificação do sistema operacional;

- [ ] Envio de email. 

---
### Bibliotecas Externas Usadas

Para construir as tabelas - [SortableTableView](https://github.com/ISchwarz23/SortableTableView):

    compile 'de.codecrafters.tableview:tableview:2.6.0'

Para fazer requisições Http - [OkHttp](https://github.com/square/okhttp):

    compile 'com.squareup.okhttp3:okhttp:3.8.1'

Para fazer o Marshalling de JSON - [Gson](https://github.com/google/gson):

    compile group: 'com.google.code.gson', name: 'gson', version: '2.8.0'

